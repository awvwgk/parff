parff
=====

[![pipeline status](https://gitlab.com/everythingfunctional/parff/badges/main/pipeline.svg)](https://gitlab.com/everythingfunctional/parff/commits/main)

Parser for Fortran. The foundations of a functional style parser combinator library.

By using a library like this, it is possible to implement parsers for complex
formats by composing independent little pieces. A good example of a project that
has made successful use of this library is
[jsonff](https://gitlab.com/everythingfunctional/jsonff).
